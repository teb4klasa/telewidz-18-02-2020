#include <iostream>
#include <string>

using namespace std;
/*
 *
 *  TV1 	film 1: od 9:00 do 12:00    3 godziny
            film 2: od 15:00 do 17:00 	2 godziny
    TV2 	film 3: od 11:00 do 16:00 	5 godzin
    TV3 	film 4: od 12:00 do 14:00 	2 godziny
    TV4 	film 5: od 11:30 do 12:30 	1 godzina
*/

struct Czas {
    Czas() {}
    Czas(int h, int m) {this->h=h;this->m=m;}
    int h,m;
};

struct Film {
    Czas start,stop;
    string name;

    int getTime() {
        int startMins= start.h*60+start.m;
        int stopMins = stop.h*60+stop.m;
        if (stopMins<startMins)
            stopMins+=24*60;
        return stopMins-startMins;
    }

    int getStartMins() {
        return start.h*60+start.m;
    }

    int getStopMins() {
        return stop.h*60+stop.m;
    }

    bool noTimeConflict(Czas cstart,Czas cstop) {
        int startMinsForegin= cstart.h*60+cstart.m;
        int stopMinsForegin=cstop.h*60+cstop.m;
        int startMins = start.h*60+start.m;
        int stopMins = stop.h*60+stop.m;
        //sprawdzamy czy potencjalny film kończy się przed czasem już dodanego filmu (bądź dokładnie o godzinie
        //rozpoczęcia już dodanego filmu) LUB czy film nie zacznie się w trakcie już trwającego filmu
        if (stopMinsForegin<=startMins ||
                startMinsForegin>=stopMins) {
             return true; //jeżeli przeszedł te warunki - jest zgdony z danym filmem na liście (nie powoduje z nim konfliktu)
        }
        //jeżeli poprzedni warunek się nie wykona - film zachodzi ze wskazanym (pretendentem) w konflikt
        return false;
    }
};

struct TV {
    Film *films;
    int filmsCount;
    string name;
};

int main()
{
    TV *tvs = new TV[4];
    tvs[0].name = "TV1";
    tvs[0].films = new Film[2];
    tvs[0].filmsCount = 2;
    tvs[0].films[0].name = "film1";
    tvs[0].films[0].start = Czas(9,0);
    tvs[0].films[0].stop = Czas(12,0);
    tvs[0].films[1].name = "film2";
    tvs[0].films[1].start = Czas(15,0);
    tvs[0].films[1].stop = Czas(17,0);

    tvs[1].name = "TV2";
    tvs[1].films = new Film[1];
    tvs[1].filmsCount = 1;
    tvs[1].films[0].name = "film3";
    tvs[1].films[0].start = Czas(11,0);
    tvs[1].films[0].stop = Czas(16,0);

    tvs[2].name = "TV3";
    tvs[2].films = new Film[1];
    tvs[2].filmsCount = 1;
    tvs[2].films[0].name = "film4";
    tvs[2].films[0].start = Czas(12,0);
    tvs[2].films[0].stop = Czas(14,0);

    tvs[3].name = "TV4";
    tvs[3].films = new Film[1];
    tvs[3].filmsCount = 1;
    tvs[3].films[0].name = "film5";
    tvs[3].films[0].start = Czas(11,30);
    tvs[3].films[0].stop = Czas(12,30);
   // cout << "Hello World!" << endl;

    //Strategia A
    Film f = tvs[0].films[0];
    int fc = 0;
    for (int i=0;i<4;i++) {
        fc+=tvs[i].filmsCount;
        for (int j=0;j<tvs[i].filmsCount;j++) {
            if ((tvs[i].films[j].getTime()>f.getTime()) ||
                   (tvs[i].films[j].getTime()==f.getTime() && tvs[i].films[j].getStopMins()<f.getStopMins()) )
                f = tvs[i].films[j];
        }
    }
    Film *retFilms = new Film[fc];
    fc =0;
    retFilms[fc++]=f;
    for (int i=0;i<4;i++) {

        for (int j=0;j<tvs[i].filmsCount;j++) {
            bool appendFilm=true;
            for (int k=0;k<fc;k++) {
                if (!tvs[i].films[j].noTimeConflict(retFilms[k].start,retFilms[k].stop))
                    appendFilm=false;
            }
            if (appendFilm)
                retFilms[fc++]=tvs[i].films[j];
        }


    }
    cout << "STRATEGIA A: " << endl;
    for (int i=0;i<fc;i++)
        cout << retFilms[i].name << endl;
    //strategia B
    f = tvs[0].films[0];
    fc = 0;
    for (int i=0;i<4;i++) {
        fc+=tvs[i].filmsCount;
        for (int j=0;j<tvs[i].filmsCount;j++) {
            if ((tvs[i].films[j].getTime()<f.getTime())||
                    (tvs[i].films[j].getTime()==f.getTime() && tvs[i].films[j].getStopMins()<f.getStopMins()))
                f = tvs[i].films[j];
        }
    }
    retFilms = new Film[fc];
    fc =0;
    retFilms[fc++]=f;
    for (int i=0;i<4;i++) {

        for (int j=0;j<tvs[i].filmsCount;j++) {
            bool appendFilm=true;
            for (int k=0;k<fc;k++) {
                if (!tvs[i].films[j].noTimeConflict(retFilms[k].start,retFilms[k].stop))
                    appendFilm=false;
            }
            if (appendFilm)
                retFilms[fc++]=tvs[i].films[j];
        }


    }
    cout << "STRATEGIA B: " << endl;
    for (int i=0;i<fc;i++)
        cout << retFilms[i].name << endl;

    //STRATEGIA C
    f = tvs[0].films[0];
    fc = 0;
    for (int i=0;i<4;i++) {
        fc+=tvs[i].filmsCount;
        for (int j=0;j<tvs[i].filmsCount;j++) {
            if ((tvs[i].films[j].getStartMins()<f.getStartMins())||
                    (tvs[i].films[j].getStartMins()==f.getStartMins() && tvs[i].films[j].getStopMins()<f.getStopMins()))
                f = tvs[i].films[j];
        }
    }
    retFilms = new Film[fc];
    fc =0;
    retFilms[fc++]=f;
    for (int i=0;i<4;i++) {

        for (int j=0;j<tvs[i].filmsCount;j++) {
            bool appendFilm=true;
            for (int k=0;k<fc;k++) {
                if (!tvs[i].films[j].noTimeConflict(retFilms[k].start,retFilms[k].stop))
                    appendFilm=false;
            }
            if (appendFilm)
                retFilms[fc++]=tvs[i].films[j];
        }


    }
    cout << "STRATEGIA C: " << endl;
    for (int i=0;i<fc;i++)
        cout << retFilms[i].name << endl;

    //STRATEGIA D
    f = tvs[0].films[0];
    fc = 0;
    for (int i=0;i<4;i++) {
        fc+=tvs[i].filmsCount;
        for (int j=0;j<tvs[i].filmsCount;j++) {
            if ((tvs[i].films[j].getStopMins()<f.getStopMins())||
                    (tvs[i].films[j].getStopMins()==f.getStopMins() && tvs[i].films[j].getStartMins()>f.getStartMins()))
                f = tvs[i].films[j];
        }
    }
    retFilms = new Film[fc];
    fc =0;
    retFilms[fc++]=f;
    for (int i=0;i<4;i++) {

        for (int j=0;j<tvs[i].filmsCount;j++) {
            bool appendFilm=true;
            for (int k=0;k<fc;k++) {
                if (!tvs[i].films[j].noTimeConflict(retFilms[k].start,retFilms[k].stop))
                    appendFilm=false;
            }
            if (appendFilm)
                retFilms[fc++]=tvs[i].films[j];
        }


    }
    cout << "STRATEGIA D: " << endl;
    for (int i=0;i<fc;i++)
        cout << retFilms[i].name << endl;
    return 0;
}
